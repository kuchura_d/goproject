package main

import (
	"fmt"
	"time"
	"net/http"
	"database/sql"
	"html/template"
	"golang.org/x/crypto/bcrypt"
	_ "github.com/go-sql-driver/mysql"
)

// Global sql database by all handlers
var db *sql.DB
var err error

func main() {
	// Create an MySQL and check for errors
	db, err = sql.Open("mysql", "root:@/golang")
	if err != nil {
		panic(err.Error())
	}
	// Close my SQL connection
	defer db.Close()

	// Test the connection to the database
	err = db.Ping()
	if err != nil {
		panic(err.Error())
	}

	// Хандлим (css, js) файлы
	http.Handle("/assets/", http.StripPrefix("/assets/", http.FileServer(http.Dir("./src/assets/"))))

	// Хандлеры для реквестов (Роутинг)
	http.HandleFunc("/", homePage)
	http.HandleFunc("/about", aboutPage)
	http.HandleFunc("/login", loginPage)

	// Тут будет новый роутинг
	//http.HandleFunc("/articles/{category}/", ArticlesCategoryHandler)
	//http.HandleFunc("/articles/{page}", ArticlePageHandler)

	http.ListenAndServe(":8080", nil)
}

func homePage(res http.ResponseWriter, req *http.Request) {
	// Index page
	t, err := template.ParseFiles("src/templates/index.html", "src/templates/header.html", "src/templates/footer.html")
	if err != nil {
		fmt.Fprintf(res, err.Error())
		return
	}

	t.ExecuteTemplate(res, "index", nil)
}

func aboutPage(res http.ResponseWriter, req *http.Request) {
	// About page
	t, err := template.ParseFiles("src/templates/about.html", "src/templates/header.html", "src/templates/footer.html")
	if err != nil {
		fmt.Fprintf(res, err.Error())
		return
	}

	t.ExecuteTemplate(res, "about", nil)
}

func loginPage(res http.ResponseWriter, req *http.Request) {
	// Login FORM
	if req.Method != "POST" {
		t, err := template.ParseFiles("src/templates/login.html", "src/templates/header.html", "src/templates/footer.html")
		if err != nil {
			fmt.Fprintf(res, err.Error())
			return
		}

		t.ExecuteTemplate(res, "login", nil)
	} else {
		// Объявление переменных из реквеста
		email := req.FormValue("email")
		password := req.FormValue("password")
		created_at := time.Now().String()

		// создание модели Юзер
		var user string

		err := db.QueryRow("SELECT email FROM users WHERE email = ?", email).Scan(&user)

		// switch я це люблю <3
		switch {
		// check Username is available
		case err == sql.ErrNoRows:
			hashedPassword, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
			if err != nil {
				http.Error(res, "Ошибка шифрования пароля.", 500)
				return
			}

			_, err = db.Exec("INSERT INTO users(email, password, created_at) VALUES(?, ?, ?)", email, hashedPassword, created_at)
			if err != nil {
				http.Error(res, "Ошибка записи в таблицу.", 500)
				res.Write([]byte(created_at))
				return
			}

			res.Write([]byte("Пользователь создан!"))
			return
		case err != nil:
			http.Error(res, "Ошибка сервера, не возможно записать данные таблицу.", 500)
			return
		default:
			http.Redirect(res, req, "/", 301)
		}
	}

}
